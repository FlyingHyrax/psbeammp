<#
.SYNOPSIS
    Show info about BeamMP "CaRP" mod community servers
.DESCRIPTION
    Shows number of online players and current PPS for CaRP community servers.
    Trims the size of the server names compared to using the full server name
    with the "Get-BeamMpServer" function to make the output more readable.
    Useful to quickly see what servers are online and how busy each server is,
    as well as the total number of connected players.
.EXAMPLE
    PS C:\> .\scripts\ListCarp.ps1

    Server             Has Space Players Max Pps ServerVersion
    ------             --------- ------- --- --- -------------
    East Coast 2           False       6   6   8 2.2.0
    East Coast             False       7   6   8 2.2.0
    Italy 2                 True       6  10  11 2.2.0
    Italy                   True       3  10  14 2.2.0
    Jungle Rock Island      True       1   8   0 2.2.0
    Utah                   False      12  12   4 2.2.0
    West Coast USA 2        True       5   8  13 2.2.0
    West Coast USA         False       9   8   6 2.2.0

    🐟 49 players online on 8 servers 🐟

.INPUTS
    n/a
.OUTPUTS
    n/a
#>
if ($null -eq (Get-Module PsBeamMp)) {
    Import-Module "$PsScriptRoot\..\PsBeamMp.psd1"
}

function extractName($server) {
    $server.name_plain -match '\]\[(.+)\]' | Out-Null
    if ($matches) {
        $matches[1]
    } else {
        $server.name_plain
    }
}
<#
$open = @{
    label="Has Space";
    expression={ if ($_.IsFull()) { "✖" } else { "✔" } }
}
#>
$open = @{
    label="Has Space";
    expression={ -not $_.IsFull() }
}

$shortname = @{
    label="Server";
    expression={ extractName -server $_ }
}

$players = @{
    label="Players";
    expression={$_.PlayerCount}
}

$max = @{
    label="Max";
    expression={$_.MaxPlayers}}

$carp_servers = Get-BeamMpServer -Name "CaRP Test Server"
$carp_servers | Sort-Object -Property Name | Select-Object -Property $shortname,$open,$players,$max,Pps,ServerVersion | Format-Table

$server_count = $carp_servers.Count
$player_count = $carp_servers | Measure-Object -Property PlayerCount -Sum | Select-Object -ExpandProperty Sum
Write-Output "🐟 $player_count players online on $server_count servers 🐟"
