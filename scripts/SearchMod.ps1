<#
.SYNOPSIS
    Show the number of BeamMP servers with a particular mod installed
.DESCRIPTION
    Filters BeamMP servers for those that have some mod installed with
    a name that matches the ModName parameter. If more than one mod
    matches the given name, shows the number of servers for each mod
    that matches.
.EXAMPLE
    PS C:\> .\scripts\SearchMod.ps1 -ModName 6x6 -InformationAction Continue
    Servers with mods matching '6x6':

    Name                          Count
    ----                          -----
    firehawk_6x6_F                    3
    6x6_dseries                       9
    {6x6_dseries, firehawk_6x6_F}     1

.INPUTS
    Does not accept pipeline input
.OUTPUTS
    Does not write objects to the pipeline
#>
[CmdletBinding()]
param (
    [Parameter(Mandatory, Position=0)]
    [string]
    $ModName
)

if ($null -eq (Get-Module PsBeamMp)) {
    Import-Module "$PsScriptRoot\..\PsBeamMp.psd1"
}

function GetModsLike($server, $modname) {
    if ($server.Mods) {
        @($server.Mods -ilike "*$modname*")
    } else {
        @()
    }
}

Write-Information "Servers with mods matching '$ModName':"

Get-BeamMpServer | ForEach-Object {
    Add-Member -InputObject $_ -PassThru -MemberType NoteProperty -Name "Matching" -Value (GetModsLike -server $_ -modname $ModName)
} | Where-Object {
    $_.Matching.Count -gt 0
} | Group-Object -Property Matching | Select-Object -Property Name,Count | Format-Table