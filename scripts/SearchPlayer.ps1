<#
.SYNOPSIS
    Search public BeamMP servers for a user
.DESCRIPTION
    Search public BeamMP servers for a player name. If a player with that name is online,
    prints information about the server the player is connected to.
.EXAMPLE
    PS C:\> .\scripts\SearchPlayer.ps1 -Name TacoCat

    TacoCat is online:

    Name        : [🐟 CaRP Test Server][West Coast USA 2]   🎯 Real Missions™️ / 💵 Actual In-Game Economy 😱 ™️ / 🎩 Capitalism™️
    Map         : west_coast_usa
    PlayerCount : 7
    MaxPlayers  : 8
    Pps         : 6
    Players     : {REV_STREAK, ferrazpena, TacoCat, XxMartyMk47xX…}
.INPUTS
    n/a
.OUTPUTS
    n/a
#>
[CmdletBinding()]
param (
    [Parameter(Mandatory, Position = 0)]
    [Alias("Player")]
    [string]
    $Name
)
    
# Import "Get-BmpServer"
if ($null -eq (Get-Module PsBeamMp)) {
    Import-Module "$PsScriptRoot\..\PsBeamMp.psd1"
}

$results = @(Get-BeamMpServer | Where-Object { $_.Players -icontains $Name })

if ($results.Count -gt 0) {
    Write-Output "$Name is online:"
    Format-List -InputObject $results -Property Name,Map,PlayerCount,MaxPlayers,Pps,Players
} else {
    Write-Output "$Name is not on a server"
}