Import-Module "$PSScriptRoot\BmpTextStyle.psm1"
Import-Module "$PSScriptRoot\VarCache.psm1"

function splitPlayerList([string]$rawPlayerList) {
    $rawPlayerList -split ';'
}

function splitModList([string]$rawModList) {
    $rawModList -split ';' | ForEach-Object { 
        if (-not [string]::IsNullOrWhiteSpace($_)) {
            $_.Substring(1)
        }
    }
}

function readPps([string]$rawPps) {
    $rawPps -eq '-' ? $null : [int]$rawPps
}

function readMapName([string]$rawMapPath) {
    $rawMapPath -match '^/levels/(.+)/' | Out-Null
    if ($matches) {
        $matches[1]
    } else {
        $rawMapPath
    }
}

class BeamMpServer {
    [string] $Name
    hidden [string] $name_plain
    
    [string[]] $Players
    [int] $MaxPlayers
    [int] $PlayerCount
    
    [Net.IPAddress] $IpAddress
    [int] $Port
    [string] $Location
    [int] $Pps
    
    [string] $Map
    [string] $Description
    hidden [string] $description_plain
    
    [Version] $ServerVersion
    [Version] $ClientVersion
    
    [string[]] $Mods
    [int] $ModCount
    [long] $ModsTotalSize
    
    [boolean] $IsOfficial
    [string] $Owner
    [datetime] $Time

    BeamMpServer(
        [object] $objectFromJson,
        [bool] $vteColors
    ) {
        $this.Players = splitPlayerList -rawPlayerList $objectFromJson.playerslist
        $this.MaxPlayers = [int]$objectFromJson.maxplayers
        $this.PlayerCount = $this.Players.Count
        
        $this.IpAddress = [Net.IPAddress]::Parse($objectFromJson.ip)
        $this.Port = [int]$objectFromJson.port
        $this.Location = $objectFromJson.location
        $this.Pps = readPps -rawPps $objectFromJson.pps

        $this.Map = readMapName -rawMapPath $objectFromJson.map
        $this.name_plain = Remove-TextStyle -Text $objectFromJson.sname
        $this.description_plain = Remove-TextStyle -Text  $objectFromJson.sdesc

        if ($vteColors) {
            $this.Name = Convert-TextStyleToANSI -Text $objectFromJson.sname
            $this.Description = Convert-TextStyleToANSI -Text $objectFromJson.sdesc
        } else {
            $this.Name = $this.name_plain
            $this.Description = $this.description_plain
        }

        $this.ServerVersion = [Version]::Parse($objectFromJson.version)
        $this.ClientVersion = [Version]::Parse($objectFromJson.cversion)
        
        $this.Mods = splitModList -rawModList $objectFromJson.modlist
        $this.ModCount = [int] $objectFromJson.modstotal
        $this.ModsTotalSize = [long] $objectFromJson.modstotalsize
        
        $this.IsOfficial = $objectFromJson.official
        $this.Owner = $objectFromJson.owner
        $this.Time = $objectFromJson.time
    }

    [bool] IsFull() {
        return $this.Players.Count -ge $this.MaxPlayers
    }

    [bool] IsEmpty() {
        return $this.Players.Count -eq 0
    }

    [bool] IsModded() {
        return $this.ModCount -gt 0
    }

    [bool] MatchName([string]$test, [bool]$matchCase) {
        if ($matchCase) {
            return $this.name_plain -clike "*$test*"
        } else {
            return $this.name_plain -ilike "*$test*"
        }
    }

    [bool] HasMod([string]$mod, [bool]$matchCase) {
        if ($matchCase) {
            return $this.Mods -ccontains $mod
        } else {
            return $this.Mods -icontains $mod
        }
    }

    [bool] HasAnyMod([string[]]$mods, [bool]$matchCase) {
        foreach ($mod in $mods) {
            if ($this.HasMod($mod, $matchCase)) {
                return $true
            }
        }
        return $false
    }

    [bool] HasAllMods([string[]]$mods, [bool]$matchCase) {
        foreach ($mod in $mods) {
            if (-not $this.HasMod($mod, $matchCase)) {
                return $false
            }
        }
        return $true
    }
}

function New-Server {
    [CmdletBinding()]
    param (
        # Object converted from JSON containing server attributes
        [Parameter(Mandatory = $true, Position = 0, ValueFromPipeline = $true)]
        [object]
        $InputObject,

        [Parameter()]
        [switch]
        $TermColor
    )
    
    process {
        Write-Output ([BeamMpServer]::new($InputObject, $TermColor))
    }
}

<#
.SYNOPSIS
    Get a list of public BeamMP servers
.DESCRIPTION
    Get a list of public game servers for "BeamMP" - the multiplayer mod for "BeamNG.drive".

    Requests server information from the BeamMP backend, parses the JSON response into
    PowerShell objects, and optionally filters the results on some common criteria. 
    
    The returned "BeamMpServer" objects can then be sorted and filtered as desired using
    standard PowerShell commands like Where-Object, Sort-Object, Select-Property, etc.
.EXAMPLE
    PS C:\> Get-BeamMpServer | Get-Member

    List the properties and methods of "BeamMpServer" objects
.EXAMPLE
    PS C:\> Get-BeamMpServer | Group-Object -Property ServerVersion | Select-Object -Property Name,Count

    Group servers by server version and show the number of servers running each version.
.INPUTS
    Does not accept pipeline input
.OUTPUTS
    One or more BeamMpServer objects
.NOTES
    General notes
#>
function Get-Server {
    [CmdletBinding()]
    param (
        # Filter by server name
        [Parameter()]
        [string]
        $Name,
        # Filter by installed mods
        [Parameter()]
        [string]
        $Mod,
        # Filter out full servers
        [Parameter()]
        [switch]
        $NotFull,
        # Filter out empty servers
        [Parameter()]
        [switch]
        $NotEmpty,
        # Match upper/lower case on server name or mod names
        [Parameter()]
        [switch]
        $MatchCase,
        # Use VT ANSI text formatting in server name and description
        [Parameter()]
        [switch]
        $TermColor
    )
    
    begin {
        $BackendURI = 'https://backend.beammp.com/servers'
        $CacheName = 'PsBeamMp_ServerList'
        $CacheTime = New-TimeSpan -Seconds 90

        function Invoke-ServerRequest {
            return (
                Invoke-WebRequest -Uri $BackendURI -Method Post
                | Select-Object -ExpandProperty Content
                | ConvertFrom-Json
            )
        }
    }
    
    process {
        $objects = Get-CachedValue -Name $CacheName
        if ($null -eq $objects) {
            Write-Debug "No cache; downloading from backend"
            $objects = Invoke-ServerRequest
            Set-CachedValue -Name $CacheName -Value $objects -Lifetime $CacheTime
        }
        
        $servers = $null
        if ($TermColor) {
            Write-Debug "-TermColor on; converting to ANSI"
            $servers = $objects | New-Server -TermColor
        } else {
            $servers = $objects | New-Server
        }

        if ($Name) {
            Write-Debug "Filtering: by name"
            $servers = $servers | Where-Object -FilterScript { $_.MatchName($Name, $MatchCase) }
        }

        if ($Mod) {
            Write-Debug "Filtering: by mod"
            $servers = $servers | Where-Object -FilterScript { $_.HasMod($Mod, $MatchCase) }
        }

        if ($NotFull) {
            Write-Debug "Filtering: no full servers"
            $servers = $servers | Where-Object -FilterScript { -not $_.IsFull() }
        }

        if ($NotEmpty) {
            Write-Debug "Filtering: no empty servers"
            $servers = $servers | Where-Object -FilterScript { -not $_.IsEmpty() }
        }

        Write-Output $servers
    }
    
    end {
        # n/a   
    }
}

Export-ModuleMember -Function New-Server,Get-Server
