using namespace System.Text.RegularExpressions

# The ANSI "ESC" byte, 0x1B (decimal 27)
$esc = [char]0x1b

# Create a simple ANSI "select graphics rendition" escape sequence
function sgr($attr) {
    return "$esc[${attr}m"
}

# Create an ANSI SGR escape sequence for 8-bit/256-color foreground color
function fgColor($code) {
    $attr = @(38, 5, $code) -join ';'
    return sgr -attr $attr
}

# Table of simple, non-color SGR formatting sequences
$Consts = [PSCustomObject]@{
    Reset     = (sgr -attr 0)
    Bold      = (sgr -attr 1)
    Italic    = (sgr -attr 3)
    Underline = (sgr -attr 4)
    Strike    = (sgr -attr 9)
    Newline   = ( @([char]0x0d, [char]0x1a) -join '' )
}

# Convert a BeamMP text formatting code to it's nearest ANSI SGR escape sequence equivalent
function bmp2ansi([string]$style) {
    switch ($style) {
        'r' { $Consts.Reset }
        'p' { $Consts.Newline }
        'n' { $Consts.Underline }
        'l' { $Consts.Bold }
        'm' { $Consts.Strike }
        'o' { $Consts.Italic }
        '0' { fgColor -code 234 }  # "black" (dark dark gray)
        '1' { fgColor -code 27 }   # blue 
        '2' { fgColor -code 34 }   # green
        '3' { fgColor -code 39 }   # light blue
        '4' { fgColor -code 124 }  # red
        '5' { fgColor -code 207 }  # pink
        '6' { fgColor -code 214 }  # orange
        '7' { fgColor -code 247 }  # gray, lighter
        '8' { fgColor -code 240 }  # gray, darker
        '9' { fgColor -code 99 }   # light purple
        'a' { fgColor -code 82 }   # light green
        'b' { fgColor -code 39 }   # light blue (again)
        'c' { fgColor -code 202 }  # dark orange
        'd' { fgColor -code 219 }  # light pink
        'e' { fgColor -code 226 }  # yellow
        'f' { fgColor -code 247 }  # "white" (light light gray)
        Default { $style }
    }
}

<#
.SYNOPSIS
    Convert BeamMP text formatting into terminal formatting
.DESCRIPTION
    Converts inline BeamMP text formatting specifiers into their corresponding
    virtual terminal ANSI escape sequences.

    In other words, this is to pretty-print BeamMP-formatted text in a terminal emulator.

    BeamMP text formatting specifiers are described here:
    <https://wiki.beammp.com/en/home/server-maintenance#customize-the-look-of-your-server-name>

    Graphics-related ANSI escape codes are described here:
    <https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters>

    Terminal colors were chosen from the 8-bit/256-color lookup table to 
    approximate the BeamMP colors by name. These will not render correctly if
    your terminal emulator does not support 256 colors.
.EXAMPLE
    PS C:\> $example = "^4 red ^r ^2 green ^r ^1 blue ^r"
    PS C:\> Convert-BeamMpTextStyleToANSI -Text $example -OutVariable styled

    red   green   blue  
    PS C:\> $styled.ToCharArray() | % { ($_ -eq [char]0x1b) ? "ESC" : $_ } | Join-String -Separator ''

    ESC[38;5;124m red ESC[0m ESC[38;5;34m green ESC[0m ESC[38;5;27m blue ESC[0m
    
    First shows an example string containing BeamMP text formatting, then converts 
    that formatting into ANSI escape sequences. (The terminal formatting that would
    appear when printing `$styled` is not preserved in comment-based help.)

    Finally, converts the escape bytes in `$styled` to the literal string "ESC", to
    show the actual string content including its ANSI SGR escape sequences.
.INPUTS
    One or more strings containing BeamMP format specifiers
.OUTPUTS
    One or more strings containing equivalent ANSI escape sequences
.NOTES
    General notes
#>
function Convert-TextStyleToANSI {
    [CmdletBinding()]
    param (
        # Text containing BeamMP text styles
        [Parameter(Mandatory = $true, Position = 0, ValueFromPipeline = $true)]
        [ValidateNotNull()]
        [string[]]
        $Text
    )
    
    process {
        # powershell can convert a script block to the Delegate 
        # type required by this Regex API:
        [Regex]::Replace($Text, '\^(\w)', {
            bmp2ansi -style $args[0].Groups[1].Value[0] 
        })
    }
}

<#
.SYNOPSIS
    Remove BeamMP text formatting specifiers from a string.
.DESCRIPTION
    Remove BeamMP text formatting specifiers e.g. "^r", "^1", etc. from strings.
    
    BeamMP text formatting specifiers are described here:
    <https://wiki.beammp.com/en/home/server-maintenance#customize-the-look-of-your-server-name>

    This function removes any substring matching '\^\w' (a literal '^' followed 
    by a 'word' character) so may remove substrings other than valid BeamMP formatting.
.EXAMPLE
    PS C:\> Remove-TextStyle -Text "^5This is pink^r ^lThis is bold^r ^5^lThis is both^r"
    "This is pink This is bold This is both"
.INPUTS
    One or more strings, to remove format specifiers from
.OUTPUTS
    One or more strings, with formatting removed
#>
function Remove-TextStyle {
    [CmdletBinding()]
    param (
        # Text containing BeamMP text styles
        [Parameter(Mandatory = $true, Position = 0, ValueFromPipeline = $true)]
        [ValidateNotNull()]
        [string[]]
        $Text
    )
    
    process {
        Write-Debug "Before: '$Text'"
        $after = $Text -replace '\^\w',''
        Write-Debug "After: '$after'"
        $after
    }
}

Export-ModuleMember -Function Remove-TextStyle,Convert-TextStyleToANSI
