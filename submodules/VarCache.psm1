
class CacheValue {
    hidden [datetime] $Created
    hidden [datetime] $Expires

    [object] $Value

    CacheValue ([object] $Value, [TimeSpan] $Lifetime) {
        $this.Created = [datetime]::Now
        $this.Expires = $this.Created.Add($Lifetime)
        $this.Value = $Value
    }

    [bool] IsExpired() {
        return [datetime]::Now -ge $this.Expires
    }

    [bool] IsValid() {
        return -not $this.IsExpired()
    }

    [string] ToString() {
        $type = $this.Value.GetType()
        $create = $this.Created.ToShortTimeString()
        $expire = $this.Expires.ToShortTimeString()
        return "$type cached at $create expires at $expire"
    }
}

<#
.SYNOPSIS
    Cache a value
.DESCRIPTION
    Store a value as a global variable with the specified name,
    wrapped in a CacheValue object with the specified lifetime.
    Accessing the cached value by name with Get-CachedValue will
    check that the variable has not been stored for longer than
    the specified lifetime.
    The 'Name' parameter is subject to collisions in the global variable 
    namespace; choose descriptive, specific cache names to avoid this.
.EXAMPLE
    PS C:\> $CacheDuration = New-TimeSpan -Minutes 5
    PS C:\> Set-CachedValue -Name "cache_mymod_myvar" -Value $SomeValue -Lifetime $CacheDuration
    This creates or replaces the value of a global variable "cache_mymod_myvar" 
    with a CacheValue object instance. Access the stored value with 'Get-CachedValue'.
.EXAMPLE
    PS C:\> $SomeValue | Set-CachedValue -Name "cache_mymod_myvar" -Lifetime $CacheDuration
    Alternative syntax using pipeline input as the value to store.
.EXAMPLE
    PS C:\> Set-CachedValue $SomeValue "cache_mymod_myvar" $CacheDuration
    Alternative syntax using positional arguments.
.INPUTS
    Pipeline input will bind to the 'Value' parameter. Note that since only one 
    cache key / name can be specified, for pipelines yielding more than one object, 
    successive objects will overwrite previous ones and only the last value will
    ultimately be stored.
.OUTPUTS
    None
.NOTES
    General notes
#>
function Set-CachedValue {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $True, Position = 0)]
        [object]
        $Value,

        [Parameter(Mandatory = $true, Position = 1)]
        [string]
        $Name,

        [Parameter(Mandatory = $true, Position = 2)]
        [TimeSpan]
        $Lifetime
    )

    process {
        $cache = [CacheValue]::new($Value, $Lifetime)
        Set-Variable -Name $Name -Value $cache -Scope Global -Description "Cached value $Name"
    }
}

<#
.SYNOPSIS
    Retrieve a cached value.
.DESCRIPTION
    Read a value previously stored with 'Set-CachedValue'. 
    If no value has been set or the stored value is expired, outputs `$null`.
.EXAMPLE
    PS C:\> $CacheKey = "cache_mymod_myvalue"
    PS C:\> $CacheLife = New-TimeSpan -Seconds 90
    PS C:\> $value = Get-CachedValue -Name $CacheKey
    PS C:\> if (-not $value) { 
    >>     $newValue = Do-SomeExpensiveOperation
    >>     Set-CachedValue -Name $CacheKey -Value $newValue -Lifetime $CacheLife
    >>     $value = $newValue
    >> }
    PS C:\> $value
    # ...your value here...
    
    Tries to retrieve a value from the cache. If the value has not been set 
    previously or is older than '$CacheLife', '$value' will be null. In that
    case we can perform the computation needed to calculate a fresh value, and
    store the new value with 'Set-CachedValue' before continuing.
.INPUTS
    Does not accept pipeline input
.OUTPUTS
    Stored cached object, or $null
#>
function Get-CachedValue {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, Position = 0)]
        $Name
    )

    process {
        $cache = Get-Variable -Name $Name -Scope Global -ValueOnly -ErrorAction Ignore
        
        if ($cache -isnot [CacheValue]) {
            return $null
        }
        
        if ($cache.IsExpired()) {
            Clear-Variable -Name $Name -Scope Global
            return $null
        }
        
        return $cache.Value
    }
}

Export-ModuleMember -Function Set-CachedValue,Get-CachedValue
